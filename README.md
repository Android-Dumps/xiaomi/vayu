## qssi-user 12
12 SKQ1.211006.001 V13.0.1.0.SJUINXM release-keys
- Manufacturer: xiaomi
- Platform: msmnile
- Codename: vayu
- Brand: Xiaomi
- Flavor: qssi-user
- Release Version: 12
12
- Id: SKQ1.211006.001
- Incremental: V13.0.1.0.SJUINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Xiaomi/vayu_in/vayu:12/RKQ1.200826.002/V13.0.1.0.SJUINXM:user/release-keys
- OTA version: 
- Branch: qssi-user-12
12-SKQ1.211006.001-V13.0.1.0.SJUINXM-release-keys
- Repo: xiaomi/vayu
