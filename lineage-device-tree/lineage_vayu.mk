#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from vayu device
$(call inherit-product, device/xiaomi/vayu/device.mk)

PRODUCT_DEVICE := vayu
PRODUCT_NAME := lineage_vayu
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := vayu_in
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="vayu_in-user 12 RKQ1.200826.002 V13.0.1.0.SJUINXM release-keys"

BUILD_FINGERPRINT := Xiaomi/vayu_in/vayu:12/RKQ1.200826.002/V13.0.1.0.SJUINXM:user/release-keys
